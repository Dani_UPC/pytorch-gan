from torchgan.models.gan import GAN
from torchgan.datasets.animals import AnimalsDataset, Rescale, RandomCrop, \
                                      ToTensor, Normalize

import torchvision.transforms as transforms


img_src = (72, 72)
img_dims = (64, 64)
batch_size = 16


dataset = AnimalsDataset(
    root_folder='/home/walle/workspace/datasets/animals',
    transform_fn=transforms.Compose(
        [
            Rescale(img_src),
            RandomCrop(img_dims),
            ToTensor(),
            Normalize(mean=[0.5, 0.5, 0.5], std=[0.5, 0.5, 0.5])
        ]
    )
)

gan_obj = GAN(dataset, img_dims)
gan_obj.train(batch_size)
