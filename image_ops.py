from skimage import transform
import numpy as np


def resize_image(image, output_size):
    """ Resizes image into given size
    Args:
        image: Numpy array with size: [Channels x Height x Width]
        output_size: Side of the image or tuple containing (height, width)
    """
    h, w = image.shape[:2]
    if isinstance(output_size, int):
        if h > w:
            new_h, new_w = output_size * h / w, output_size
        else:
            new_h, new_w = output_size, output_size * w / h
    else:
        new_h, new_w = output_size

    new_h, new_w = int(new_h), int(new_w)
    return transform.resize(image, (new_h, new_w))


def random_crop(image, output_size):
    """ Crops randomly around the center of the image with the given size """
    h, w = image.shape[:2]
    new_h, new_w = output_size

    top = np.random.randint(0, h - new_h)
    left = np.random.randint(0, w - new_w)

    return image[top:(top + new_h), left:(left + new_w)]


def normalize(image, means, stds):
    """ Normalizes the image channels as

        channel = (channel - mean_channel) / std_channel
    """
    nchannels = image.shape[0]
    assert len(means) == nchannels and len(stds) == nchannels

    for c in range(nchannels):
        image[..., c] = (image[..., c] - means[c]) / stds[c]
