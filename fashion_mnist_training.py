from torchgan.models import gan
from torchgan.datasets.fashion_mnist import FashionMnistDataset
from torchgan.utils import DataMode

from torchgan.models.discriminator import FMnistDiscArch
from torchgan.models.generator import FMnistGenArch

import torchvision.transforms as transforms


batch_size = 16


dataset = FashionMnistDataset(
        root_folder='/home/walle/workspace/datasets/fashion_mnist',
        data_mode=DataMode.TRAINING,
        transform_fn=transforms.Compose(
            [
                transforms.ToTensor(),
                transforms.Normalize(mean=[0.5, 0.5, 0.5], std=[0.5, 0.5, 0.5])
            ]
        )
    )


discriminator = FMnistDiscArch()
generator = FMnistGenArch()

gan_obj = gan.GAN(dataset, discriminator.get_image_size())
gan_obj.train(batch_size,
              disc_arch=discriminator,
              gen_arch=generator)
