from animals import AnimalsDataset, Normalize, RandomCrop, Rescale, ToTensor
import torchvision.transforms as transforms

img_size_start = 150
img_size = 128

transformed_dataset = AnimalsDataset(
    root_folder='/home/walle/workspace/datasets/animals',
    transform_fn=transforms.Compose(
        [
            Rescale(150),
            RandomCrop(128),
            ToTensor(),
            Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
        ]
    )
)

for i in range(len(transformed_dataset)):
    sample = transformed_dataset[i]
    print('Class: {}'.format(sample['class']))
    print('Attrs: {}'.format(sample['attrs']))
    print('Img: {}'.format(sample['image'].size()))
    if i == 5:
        break
