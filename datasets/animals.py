import torch

import numpy as np
from skimage import io

from torchgan.utils import get_subfolders, get_files, read_tab_map
from torchgan.image_ops import resize_image, random_crop, normalize

from torch.utils.data import Dataset, DataLoader
from torchvision import transforms

import os
import logging

# Data vailable at https://cvml.ist.ac.at/AwA2/
# TODO: automatic donwload function

# How to prepare Animals for splitting: create function that divide the
# instances into 3 sets and then create subfolders train, validate and testing


logger = logging.getLogger(__name__)


class AnimalsDataset(Dataset):

    TO_EXCLUDE_IMGS = ['collie_10718.jpg']

    def __init__(self, root_folder, transform_fn=None):
        self._root_folder = root_folder
        self._read_metadata()
        self._transform_fn = transform_fn

    def _read_metadata(self):
        """ Reads and stores the metadata of all instances: class, path and
        attributes """
        self.paths, self.labels = [], []
        for folder in self._get_image_folders():
            label = os.path.basename(folder)
            for img_name in get_files(folder):

                if os.path.basename(img_name) in self.TO_EXCLUDE_IMGS:
                    logger.debug('Found excluded image %s' % img_name)
                    continue

                self.paths.append(img_name)
                self.labels.append(label)

        self._class_to_id = _read_class_mapping(self._root_folder)
        self._class_to_attr = _load_attr_class_matrix(self._root_folder)

    def _get_image_folders(self):
        return get_subfolders(os.path.join(self._root_folder, 'JPEGImages'))

    def __len__(self):
        return len(self.paths)

    def __getitem__(self, idx):
        label = self._class_to_id[self.labels[idx]] - 1
        sample = {
            'image': io.imread(self.paths[idx]),
            'class': label,
            'attrs': self._class_to_attr[label, :]
        }

        if self._transform_fn:
            sample = self._transform_fn(sample)

        return sample

    def get_n_channels(self):
        return 3

    def sample_to_image(self, sample):
        return sample['image']


def _read_class_mapping(data_path):
    """ Returns a dictionary that maps each class into its numeric id """
    map_path = os.path.join(data_path, 'classes.txt')
    return read_tab_map(map_path)


def _read_attr_mapping(data_path):
    """ Returns a dictionary that maps each attribute into
    its numeric id """
    map_path = os.path.join(data_path, 'predicates.txt')
    return read_tab_map(map_path)


def _load_attr_class_matrix(data_path):
    """ Returns the matrix [N, M] mapping N classes to M attributes """
    attr_to_class_path = os.path.join(data_path,
                                      'predicate-matrix-binary.txt')
    return np.loadtxt(attr_to_class_path).astype(int)


# Transforms extracted from:
# http://pytorch.org/tutorials/beginner/data_loading_tutorial.html


class Rescale(object):
    """Rescale the image in a sample to a given size.
    Args:
        output_size (tuple or tuple): Desired output size. If tuple, output is
            matched to output_size. If int, smaller of image edges is matched
            to output_size keeping aspect ratio the same.
    """

    def __init__(self, output_size):
        assert isinstance(output_size, (int, tuple))
        self.output_size = output_size

    def __call__(self, sample):
        return {
            'image': resize_image(sample['image'], self.output_size),
            'class': sample['class'],
            'attrs': sample['attrs']
        }


class RandomCrop(object):
    """Crop randomly the image in a sample.
    Args:
        output_size (tuple or int): Desired output size. If int, square crop
            is made.
    """

    def __init__(self, output_size):
        assert isinstance(output_size, (int, tuple))
        if isinstance(output_size, int):
            self.output_size = (output_size, output_size)
        else:
            assert len(output_size) == 2
            self.output_size = output_size

    def __call__(self, sample):
        return {
            'image': random_crop(sample['image'], self.output_size),
            'class': sample['class'],
            'attrs': sample['attrs']
        }


class Normalize(object):
    """Normalizes data channel-wise using the given mean and std
    Args:
        mean: Per channel mean
        std: Per channel std
    """

    def __init__(self, mean, std):
        assert isinstance(mean, (list, tuple))
        self.means = mean
        self.stds = std

    def __call__(self, sample):
        image, label, attrs = sample['image'], sample['class'], sample['attrs']
        normalize(image, self.means, self.stds)
        return {
            'image': image,
            'class': label,
            'attrs': attrs
        }


class ToTensor(object):
    """Convert ndarrays in sample to Tensors."""

    def __call__(self, sample):
        image, label, attrs = sample['image'], sample['class'], sample['attrs']

        # swap color axis because
        # numpy image: H x W x C
        # torch image: C X H X W
        image = image.transpose((2, 0, 1))

        return {
            'image': torch.from_numpy(image),
            'class': label,
            'attrs': torch.from_numpy(attrs)
        }


if __name__ == '__main__':

    '''
    dataset = AnimalsDataset('/home/walle/workspace/datasets/animals')

    # Show random instance
    fig = plt.figure()
    sample = dataset[25000]
    print('Class: %d' % sample['class'])
    print('Attributes: {}'.format(sample['attrs']))
    plt.imshow(sample['image'])
    plt.show()

    # Transform image
    scale = Rescale(256)
    crop = RandomCrop(128)
    composed = transforms.Compose([Rescale(140),
                                   RandomCrop(128)])

    # Apply each of the above transforms on sample.
    fig = plt.figure()
    sample = dataset[65]

    for i, tsfrm in enumerate([scale, crop, composed]):
        transformed_sample = tsfrm(sample)

        print(transformed_sample)

        ax = plt.subplot(1, 3, i + 1)
        plt.tight_layout()
        ax.set_title(type(tsfrm).__name__)
        plt.imshow(transformed_sample['image'])
        plt.show()
    '''

    # Apply batching on dataset
    transformed_dataset = AnimalsDataset(
        root_folder='/home/walle/workspace/datasets/animals',
        transform_fn=transforms.Compose(
            [
                Rescale(140),
                RandomCrop(128),
                ToTensor(),
                Normalize(mean=[0.5, 0.5, 0.5], std=[0.5, 0.5, 0.5])
            ]
        )
    )

    dataloader = DataLoader(transformed_dataset,
                            batch_size=4,
                            shuffle=True,
                            num_workers=4)

    for i_batch, sample_batched in enumerate(dataloader):

        print('Showing batch %i' % i_batch)

        print(sample_batched['image'],
              sample_batched['class'],
              sample_batched['attrs'])

        # observe 4th batch and stop.
        if i_batch == 1:
            break
