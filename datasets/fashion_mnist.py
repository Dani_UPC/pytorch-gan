from torchgan.utils import DataMode, create_dir

from torch.utils.data import Dataset, DataLoader
from torchvision import transforms
from torchvision.datasets import mnist

import matplotlib.pyplot as plt
import struct
import os
import tempfile
import array
import logging
import urllib
import gzip
from PIL import Image
import numpy as np


logger = logging.getLogger(__name__)


# More info available at: https://github.com/zalandoresearch/fashion-mnist
URLS = {
    DataMode.TRAINING: {
        'images': "http://fashion-mnist.s3-website.eu-central-1.amazonaws.com/train-images-idx3-ubyte.gz",  # noqa
        'labels': "http://fashion-mnist.s3-website.eu-central-1.amazonaws.com/train-labels-idx1-ubyte.gz"  # noqa
    },
    DataMode.TESTING: {
        "http://fashion-mnist.s3-website.eu-central-1.amazonaws.com/t10k-images-idx3-ubyte.gz",  # noqa
         "http://fashion-mnist.s3-website.eu-central-1.amazonaws.com/t10k-labels-idx1-ubyte.gz"  # noqa
    }
}

VALIDATION_RATIO = 0.10


class FashionMnistDataset(Dataset):

    def __init__(self, root_folder, data_mode, transform_fn=None):
        self._root_folder = root_folder
        self._data_mode = data_mode
        self._transform_fn = transform_fn
        self._read_dataset()

    def _read_dataset(self):
        """ Reads the corresponding dataset from the folder according to the
        data partition to read. Downloads it if it does not exist """
        self._check_downloaded()
        if self._data_mode in [DataMode.TRAINING, DataMode.VALIDATION]:
            self.images, self.labels = self._read_subset(DataMode.TRAINING)
        elif self._data_mode == DataMode.TESTING:
            self.images, self.labels = self._read_subset(DataMode.TESTING)
        else:
            raise ValueError('Uknown data subset %s' % self._data_mode)

        if self._data_mode == DataMode.VALIDATION:
            # Each time is taken a random fraction of training
            n = self.len(self.images)
            idxs = np.random.permutation(n)
            self.images = self.images[idxs]
            self.labels = self.labels[idxs]

    def _read_subset(self, data_mode):
        subfolder = os.path.join(self._root_folder, data_mode)
        images_path = os.path.join(
            subfolder,
            self._get_images_filename(data_mode)
        )
        labels_path = os.path.join(
            subfolder,
            self._get_labels_filename(data_mode)
        )
        return read_dataset(images_path, labels_path)

    def _check_downloaded(self):
        if self._data_mode in [DataMode.TRAINING, DataMode.VALIDATION]:
            if not self._exists_set(DataMode.TRAINING):
                logger.info('FashionMNIST: Training not found. Downloading...')
                create_dir(self._root_folder)
                self._download(DataMode.TRAINING)
        else:
            if not self._exists_set(DataMode.TESTING):
                logger.info('FashionMNIST: Testing not found. Downloading...')
                create_dir(self._root_folder)
                self._download(DataMode.TESTING)

    def _get_data_folder(self, data_mode):
        return os.path.join(self._root_folder, data_mode)

    def _get_images_filename(self, data_mode):
        return os.path.basename(URLS[data_mode]['images'])[:-3]

    def _get_labels_filename(self, data_mode):
        return os.path.basename(URLS[data_mode]['labels'])[:-3]

    def _exists_set(self, data_mode):
        return os.path.isdir(os.path.join(self._root_folder, data_mode))

    def _download(self, data_mode):
        subfolder = self._get_data_folder(data_mode)
        create_dir(subfolder)

        images_filename = self._get_images_filename(data_mode)
        download_and_extract(URLS[data_mode]['images'],
                             os.path.join(subfolder, images_filename))

        labels_filename = self._get_labels_filename(data_mode)
        download_and_extract(URLS[data_mode]['labels'],
                             os.path.join(subfolder, labels_filename))

    def __len__(self):
        return len(self.images)

    def __getitem__(self, idx):

        img, label = self.images[idx], self.labels[idx]
        img = np.expand_dims(img, axis=2)  # Add channel dimension of 1

        if self._transform_fn is not None:
            img = self._transform_fn(img)

        return img, label

    def get_n_channels(self):
        return 1

    def sample_to_image(self, sample):
        data, label = sample
        return data


def read_raw_data(path):
    data_params = [16, "B", ">IIII"]
    return read_raw_array(path, *data_params)


def read_raw_labels(path):
    label_params = [8, "b", ">II"]
    return read_raw_array(path, *label_params)


def read_dataset(data_path, labels_path, rows=28, cols=28):
    """ Reads raw data into numpy arrays """
    raw_data, _ = read_raw_data(data_path)
    raw_labels, _ = read_raw_labels(labels_path)

    n = len(raw_labels)
    images = np.zeros((n, rows, cols), dtype=np.int32)
    labels = np.zeros((n, 1), dtype=np.int32)
    for i in range(n):
        images[i] = np.array(raw_data[i * rows * cols:(i + 1) * rows * cols]) \
            .reshape((rows, cols))
        labels[i] = raw_labels[i]

    return images, labels


def download_and_extract(url, dst):
    """ Downloads the given url and extracts it in the desired location """

    # Download into temp file
    fd, down = tempfile.mkstemp()
    urllib.request.urlretrieve(url, down)

    # Move bytes from input to output
    with gzip.open(down, 'rb') as infile:
        with open(dst, 'wb') as outfile:
            for line in infile:
                outfile.write(line)
    # Delete tmp file
    os.close(fd)
    os.remove(down)


def read_raw_array(path, size, type, unpack_format):
    """ Reads an raw array according to its format """
    with open(path, 'rb') as f:
        metadata = struct.unpack(unpack_format, f.read(size))
        ret = array.array(type, f.read())
    return ret, metadata


if __name__ == '__main__':

    dataset = FashionMnistDataset(
        '/home/walle/workspace/datasets/fashion_mnist',
        DataMode.TRAINING
    )

    # Show random instance
    fig = plt.figure()
    sample = dataset[25000]
    print('Class: %d' % sample[1])
    print(sample[0].shape)
    plt.imshow(np.squeeze(sample[0]))
    plt.show()

    '''# Apply batching on dataset
    transformed_dataset = FashionMnistDataset(
        root_folder='/home/walle/workspace/datasets/fashion_mnist',
        data_mode=DataMode.TRAINING,
        transform_fn=transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
        ])
    )

    dataloader = DataLoader(transformed_dataset,
                            batch_size=4,
                            shuffle=True,
                            num_workers=4)

    for i_batch, sample_batched in enumerate(dataloader):

        print('Showing batch %i' % i_batch)

        print(sample_batched[1],
              sample_batched[0])

        # observe 4th batch and stop.
        if i_batch == 1:
            break
    '''