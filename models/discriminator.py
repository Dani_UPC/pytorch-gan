import torch
import torch.nn as nn

from . import DArchitecture

import logging


logger = logging.getLogger(__name__)


# Alexnet is defined here:
# https://github.com/pytorch/vision/blob/master/torchvision/models/alexnet.py


class Discriminator(nn.Module):

    def __init__(self, ngpu, disc_fn, **disc_params):
        super(Discriminator, self).__init__()
        self.ngpu = ngpu
        self.main = disc_fn(**disc_params)

    def forward(self, x):
        if isinstance(x.data, torch.cuda.FloatTensor) and self.ngpu > 1:
            output = nn.parallel.data_parallel(self.main,
                                               x,
                                               range(self.ngpu))
        else:
            output = self.main(x)

        return output.view(-1, 1).squeeze(1)


class DBaseArch(DArchitecture):

    def get_image_size(self):
        return (64, 64)

    def discriminate(self, n_channels, n_outputs=1, **params):
        relu_thresh = params.get('relu_thresh', 0.2)

        return nn.Sequential(
            # Input size: n_channels x 64 x 64
            nn.Conv2d(n_channels, 64, 4, 2, 1, bias=False),
            nn.LeakyReLU(0.2, inplace=True),

            # Input size: 64 x 32 x 32
            nn.Conv2d(64, 128, 4, 2, 1, bias=False),
            nn.BatchNorm2d(128),
            nn.LeakyReLU(relu_thresh, inplace=True),

            # Input size: 128 x 16 x 16
            nn.Conv2d(128, 256, 4, 2, 1, bias=False),
            nn.BatchNorm2d(256),
            nn.LeakyReLU(relu_thresh, inplace=True),

            # Input size: 256 x 8 x 8
            nn.Conv2d(256, 512, 4, 2, 1, bias=False),
            nn.BatchNorm2d(512),
            nn.LeakyReLU(0.2, inplace=True),

            # Input size: 512 x 4 x 4
            nn.Conv2d(512, n_outputs, 4, 1, 0, bias=False),

            # Result here is
            nn.Sigmoid()
        )


'''class FMnistDiscArch(DArchitecture):

    def get_image_size(self):
        return (28, 28)

    def discriminate(self, n_channels, n_outputs=1, **params):
        relu_thresh = params.get('relu_thresh', 0.2)
        return nn.Sequential(
            # Input size: n_channels x 28 x 28
            nn.Conv2d(n_channels, 64, 4, 2, 1, bias=False),
            nn.LeakyReLU(relu_thresh, inplace=True),

            # Input size: 64 x 14 x 14
            nn.Conv2d(64, 128, 4, 2, 1, bias=False),
            nn.BatchNorm2d(128),
            nn.LeakyReLU(relu_thresh, inplace=True),

            # Input size: 128 x 7 x 7
            nn.Conv2d(128, 1, 4, 1, dilation=2, bias=False),
            nn.Sigmoid()
        )
'''


class FMnistDiscArch(DArchitecture):

    def get_image_size(self):
        return (28, 28)

    def discriminate(self, n_channels, n_outputs=1, **params):
        relu_thresh = params.get('relu_thresh', 0.2)
        return nn.Sequential(

            # Conv2d output size is:
            # ((in + 2padding - dilation * (ks-1) -1) / stride) + 1
            # order is: in, out, ks, stride, padding
            # Default dilation is 1

            # Input size: n_channels x 28 x 28
            nn.Conv2d(n_channels, 512, 9, 1, 0, bias=False),
            nn.LeakyReLU(relu_thresh, inplace=True),

            # Input size: n_channels x 20 x 20
            nn.Conv2d(512, 256, 9, 1, 0, bias=False),
            nn.BatchNorm2d(256),
            nn.LeakyReLU(relu_thresh, inplace=True),

            # Input size: n_channels x 12 x 12
            nn.Conv2d(256, 128, 9, 1, 0, bias=False),
            nn.BatchNorm2d(128),
            nn.LeakyReLU(relu_thresh, inplace=True),

            # Input size: 128 x 4 x 4
            nn.Conv2d(128, 1, 4, 1, 0, bias=False),
            nn.Sigmoid()
        )
