import torch
import torch.nn as nn

from . import GArchitecture

# Interesting functions related

# register_backward_hook(hook) -> on gradient update
# register_forward_hook(hook) -> on forward computation


class Generator(nn.Module):

    def __init__(self, ngpu, gen_fn, **gen_params):
        super(Generator, self).__init__()
        self.ngpu = ngpu
        self.main = gen_fn(**gen_params)

    def forward(self, x):
        if isinstance(x.data, torch.cuda.FloatTensor) and self.ngpu > 1:
            output = nn.parallel.data_parallel(self.main,
                                               x,
                                               range(self.ngpu))
        else:
            output = self.main(x)

        return output


class GBaseArch(GArchitecture):

    def get_image_size(self):
        return (64, 64)

    def generate(self, input_dims, img_channels, **params):
        return nn.Sequential(

            # Input is input_dims x 1
            nn.ConvTranspose2d(input_dims, 512, 4, 1, 0, bias=False),
            nn.BatchNorm2d(512),
            nn.ReLU(True),

            # Input is 512 x 4 x 4
            nn.ConvTranspose2d(512, 256, 4, 2, 1, bias=False),
            nn.BatchNorm2d(256),
            nn.ReLU(True),

            # Input is 256 x 8 x 8
            nn.ConvTranspose2d(256, 128, 4, 2, 1, bias=False),
            nn.BatchNorm2d(128),
            nn.ReLU(True),

            # Input is 128 x 16 x 16
            nn.ConvTranspose2d(128, 64, 4, 2, 1, bias=False),
            nn.BatchNorm2d(64),
            nn.ReLU(True),

            # Input is 64 x 32 x 32
            nn.ConvTranspose2d(64, img_channels, 4, 2, 1, bias=False),
            nn.Tanh()

            # Output is img_channels x 64 x 64
        )

'''
class FMnistGenArch(GArchitecture):

    def get_image_size(self):
        return (28, 28)

    def generate(self, input_dims, img_channels, **params):
        return nn.Sequential(

            # Output is input_dims x 7 x 7
            nn.ConvTranspose2d(input_dims, 512, 7, 1, 0, bias=False),
            nn.BatchNorm2d(512),
            nn.ReLU(True),

            # Output is 512 x 14 x 14
            nn.ConvTranspose2d(512, 256, 4, 2, 1, bias=False),
            nn.BatchNorm2d(256),
            nn.ReLU(True),

            # Output is 256 x 28 x 28
            nn.ConvTranspose2d(256, img_channels, 4, 2, 1, bias=False),
            nn.Tanh()

        )
'''


class FMnistGenArch(GArchitecture):

    def get_image_size(self):
        return (28, 28)

    def generate(self, input_dims, img_channels, **params):
        return nn.Sequential(

            # Deconv size is:
            # (in-1) * stride - 2padding + ks + output_padding
            # in, out, ks, stride, padding

            # Output is 1 x 7 x 7
            nn.ConvTranspose2d(input_dims, 64, 7, 1, 0, bias=False),
            nn.BatchNorm2d(64),
            nn.ReLU(True),

            # Output is 1 x 13 x 13
            nn.ConvTranspose2d(64, 128, 7, 1, 0, bias=False),
            nn.BatchNorm2d(128),
            nn.ReLU(True),

            # Output is 1 x 19 x 19
            nn.ConvTranspose2d(128, 128, 7, 1, 0, bias=False),
            nn.BatchNorm2d(128),
            nn.ReLU(True),

            # Output is 1 x 25 x 15
            nn.ConvTranspose2d(128, 128, 7, 1, 0, bias=False),
            nn.BatchNorm2d(128),
            nn.ReLU(True),

            nn.ConvTranspose2d(128, img_channels, 4, 1, 0, bias=False),
            nn.Tanh()
        )


class FMnistUGenArch(GArchitecture):

    """ It is a encoder decoder architecture with skip connections
    (concatenation) between layer 1 and layer n - i """

    def get_image_size(self):
        return (28, 28)

    def generate(self, input_dims, img_channels, **params):
        return nn.Sequential(

            # Deconv size is:
            # (in-1) * stride - 2padding + ks + output_padding
            # in, out, ks, stride, padding

            # Output is 1 x 2 x 2
            nn.ConvTranspose2d(input_dims, 64, 4, 2, 1, bias=False),
            nn.BatchNorm2d(64),
            nn.ReLU(True),

            # Output is 64 x 4 x 4
            nn.ConvTranspose2d(64, 128, 4, 2, 1, bias=False),
            nn.BatchNorm2d(128),
            nn.ReLU(True),

            # Output is 1 x 19 x 19
            nn.ConvTranspose2d(128, 128, 7, 1, 0, bias=False),
            nn.BatchNorm2d(128),
            nn.ReLU(True),

            # Output is 1 x 25 x 15
            nn.ConvTranspose2d(128, 128, 7, 1, 0, bias=False),
            nn.BatchNorm2d(128),
            nn.ReLU(True),

            nn.ConvTranspose2d(128, img_channels, 4, 1, 0, bias=False),
            nn.Tanh()
        )