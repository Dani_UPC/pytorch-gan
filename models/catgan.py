from __future__ import print_function
import logging

import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.utils.data
from torch.autograd import Variable

from torchgan.gan import GAN

cudnn.benchmark = True

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class CATGAN(GAN):

    def __init__(self, dataset, img_dims, categories, use_cuda=True, ngpu=1):
        super(CATGAN, self).__init__(dataset, img_dims, use_cuda, ngpu)
        self._n_outputs = categories

    def _loss_g(self, logits_fake):
        loss_G = self._loss(logits_fake,
                            Variable(self._labels.fill_(1)),
                            is_generator=True)
        loss_G.backward()
        return loss_G

    # TODO modify
    def _loss_fn(self):
        return nn.CATGANLoss()

    # TODO modify
    def _loss_d(self, logits_real, logits_fake):
        # Apply gradients of true data
        d_loss_true = self._loss(logits_real,
                                 Variable(self._labels),
                                 is_generator=False)
        d_loss_true.backward()

        # Apply gradients for fake data
        d_loss_fake = self._loss(logits_fake,
                                 Variable(self._labels.fill_(0)))
        d_loss_fake.backward()

        return d_loss_true + d_loss_fake


class CATGANLoss(nn.Module):
    """ 
    
    Based on paper:

    Usupervised and semi-supervised learning categorical
    generative adversarial networks. Jost Tobias Springenberg. ICLR 2016

    """

    def forward(self, logits_real, logits_fake, is_generator):
        """ It assumes discriminator classifies into a set of closed
        categories and that it must be certain in those assigmnets coming from
        real data (low entropy) while be uncertain for assignmnets coming from
        generated data (high entropy)
        """
        if is_generator:
            return self._entropy(logits_fake) \
                - self._marginal_entropy(logits_fake)
        else:
            return self._entorpy(logits_fake) - \
                self._marginal_entropy(logits_real) - \
                self.entropy(logits_real)

    def _entropy(self, x, min_value=1e-8):
        """ Computes the expected entropy over all instances in the input.
        Each instance entropy is computed over the entropy of each category.
        Input x is expected to be a one-hot tensor of size [batch, categories]
        """
        # Sum some minimal value to all probabilities to avoid
        # computing log of 0
        non_zero_x = torch.mul(torch.ones_like(x), min_value)
        log_prob = torch.log(non_zero_x)
        instance_wise = torch.sum(torch.mult(non_zero_x, log_prob), dim=1)
        return -torch.mean(instance_wise)

    def _marginal_entropy(self, x, min_value=1e-8):
        """ Computes the marginal entropy over the inputs """
        cat_mean = torch.mean(x, dim=0)
        cat_mean = cat_mean + min_value
        return -torch.sum(cat_mean * torch.log(cat_mean))
