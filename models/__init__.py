import abc


class DArchitecture(object):

    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def get_image_size(self):
        """ Returns the expected image size of the model.
        Returns None if any """

    @abc.abstractmethod
    def discriminate(self, n_channels, n_outputs=1, **params):
        """ Returns the function to execute by the model """


class GArchitecture(object):

    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def get_image_size(self):
        """ Returns the expected image size of the model.
        Returns None if any """

    @abc.abstractmethod
    def generate(self, input_dims, img_channels, **params):
        """ Returns the function to execute by the model """
