from __future__ import print_function
import logging
import os
import numpy as np

import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim as optim
import torch.utils.data
from torch.autograd import Variable

import torchvision.utils as vutils

from torchgan.init_ops import base_init_op
from torchgan.utils import get_output_folder
from torchgan.serialize import load_context
from torchgan.models.discriminator import Discriminator, DBaseArch
from torchgan.models.generator import Generator, GBaseArch

cudnn.benchmark = True

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


LOG_STEP_TEMPLATE = '[{epoch}/{max_epoch}][{step}/{max_step}] ' + \
                    'Loss_D: {d_loss} Loss_G: {g_loss} ' + \
                    'D(x): {d_mean} D(G(z)): {d_gen_mean}'


class GAN(object):

    def __init__(self, dataset, img_dims, use_cuda=True, ngpu=1):
        self._dataset = dataset
        self._img_dims = img_dims
        self._cuda = use_cuda
        self._ngpu = ngpu
        self._n_outputs = 1

    def train(self,
              batch_size,
              n_epochs=100,
              output_dir=get_output_folder(),
              gen_n_dims=100,
              disc_arch=DBaseArch(),
              gen_arch=GBaseArch(),
              **params):
        """ TODO ARGS """
        num_workers = params.get('num_workers', 2)
        init_ops = params.get('init_ops', base_init_op)

        self._loader = torch.utils.data.DataLoader(
            self._dataset,
            batch_size=batch_size,
            shuffle=True,
            # pin_memory=True, # TODO Check
            num_workers=int(num_workers)
        )

        self._define_tensors(batch_size, gen_n_dims)

        self._define_generator(gen_arch, gen_n_dims, init_ops)
        self._define_discriminator(disc_arch, init_ops)

        # Load latest model in output
        self._load_context(output_dir)

        # Define loss function
        self._loss = self._loss_fn()

        if self._cuda:
            self._enable_gpu()

        self._define_optimizers()

        for epoch in range(self._epoch_num, n_epochs):

            for i, data in enumerate(self._loader, 0):

                # Get original data
                img_data = self._dataset.sample_to_image(data).float()
                if self._cuda:
                    img_data = img_data.cuda()

                # Get generated data
                loaded_insts = img_data.size(0)
                self._z.resize_(loaded_insts, gen_n_dims, 1, 1).normal_(0, 1)
                generated_data = self._generator(Variable(self._z))

                # Train both models
                self._update_models(img_data, generated_data)

                self._track_training_step(i, epoch, n_epochs, **params)
                self._track_training_images(i, epoch, img_data, output_dir, **params) # noqa

            self._track_training_epoch(epoch, output_dir)

    def _update_models(self, data, fake_data):
        """ TODO: improve and undestand the resize, detach, etc"""

        batch_size = data.size(0)

        ############################
        # (1) Update D network: maximize log(D(x)) + log(1 - D(G(z)))
        ###########################

        self._discriminator.zero_grad()

        # Compute discriminator loss for real data
        self._x.resize_as_(data).copy_(data)
        self._labels.resize_(batch_size).fill_(1)
        self._logits_real = self._discriminator(Variable(self._x))
        self._logits_fake = self._discriminator(fake_data.detach())
        self._loss_D = self._loss_d(self._logits_real, self._logits_fake)
        self._optimizerD.step()

        ############################
        # (2) Update G network: maximize log(D(G(z)))
        ###########################

        self._generator.zero_grad()

        # Generator wants to fool discriminator
        self._logits_fake_g = self._discriminator(fake_data)
        self._loss_G = self._loss_g(self._logits_fake_g)
        self._optimizerG.step()

    def _define_tensors(self, batch_size, gen_n_dims):
        self._x = torch.FloatTensor(
            batch_size,
            self._dataset.get_n_channels(),
            self._img_dims[0],
            self._img_dims[1]
        )
        # noise original
        self._z = torch.FloatTensor(batch_size, gen_n_dims, 1, 1)
        self._fixed_z = torch.FloatTensor(batch_size, gen_n_dims, 1, 1).normal_(0, 1)  # noqa
        self._labels = torch.FloatTensor(batch_size)

    def _enable_gpu(self):
        self._discriminator.cuda()
        self._generator.cuda()
        self._loss.cuda()
        self._x = self._x.cuda()
        self._labels = self._labels.cuda()
        self._z, self._fixed_z = self._z.cuda(), self._fixed_z.cuda()

    def _define_generator(self, arch, n_dims, init_op):
        self._generator = Generator(
            self._ngpu,
            gen_fn=arch.generate,
            input_dims=n_dims,
            img_channels=self._dataset.get_n_channels()
        )
        self._generator.apply(init_op)

    def _define_discriminator(self, arch, init_op):
        self._discriminator = Discriminator(
            self._ngpu,
            disc_fn=arch.discriminate,
            n_channels=self._dataset.get_n_channels(),
            n_outputs=self._n_outputs
        )
        self._generator.apply(init_op)

    def _load_context(self, output_dir):
        logger.info('Checking output folder...')
        nets_context = load_context(get_output_folder())
        if nets_context is not None:
            self._epoch_num, gen_obj, disc_obj = nets_context
            logger.info('Loading network from epoch %d' % self._epoch_num)
            self._generator.load_state_dict(gen_obj)
            self._discriminator.load_state_dict(disc_obj)
        else:
            self._epoch_num = 0

    def _define_optimizers(self, **params):
        beta1, beta2 = params.get('beta1', 0.5), params.get('beta2', 0.999)
        d_lr, g_lr = params.get('d_lr', 1e-4), params.get('d_lr', 1e-4)
        self._optimizerD = optim.Adam(
            self._discriminator.parameters(),
            lr=d_lr,
            betas=(beta1, beta2)
        )
        self._optimizerG = optim.Adam(
            self._generator.parameters(),
            lr=g_lr,
            betas=(beta1, beta2)
        )

    def _track_training_step(self, i, epoch, max_epoch, **params):
        # We want to log each training step
        logger.info(
            LOG_STEP_TEMPLATE.format(
                epoch=epoch,
                max_epoch=max_epoch,
                step=i,
                max_step=len(self._loader),
                d_loss=np.around(self._loss_D.data[0], 3),
                g_loss=np.around(self._loss_G.data[0], 3),
                d_mean=np.around(self._logits_real.data.mean(), 3),
                d_gen_mean=np.around(self._logits_fake.data.mean(), 3)
            )
        )

    def _track_training_images(self, i, epoch, data, output_dir, **params):
        # Save images into output
        save_img_interval = params.get('track_images_int', 500)
        if i % save_img_interval == 0:
            # Save original data
            vutils.save_image(
                data,
                os.path.join(output_dir, 'real_samples.png'),
                normalize=True
            )

            # Save generated data
            fake = self._generator(Variable(self._fixed_z))
            vutils.save_image(
                fake.data,
                os.path.join(output_dir, 'fake_samples_epoch_%03d.png') % epoch,  # noqa
                normalize=True)

    def _track_training_epoch(self, epoch, output_dir, **params):
        save_model_interval = params.get('save_model_int', 5)
        if epoch % save_model_interval == 0:
            torch.save(self._generator.state_dict(),
                       os.path.join(output_dir, 'netG_epoch_%d.pth') % epoch)
            torch.save(self._discriminator.state_dict(),
                       os.path.join(output_dir, 'netD_epoch_%d.pth') % epoch)

    def _loss_g(self, logits_fake):
        loss_G = self._loss(logits_fake, Variable(self._labels.fill_(1)))
        loss_G.backward()
        return loss_G

    def _loss_fn(self):
        return nn.BCELoss()

    def _loss_d(self, logits_real, logits_fake):
        # Apply gradients of true data
        d_loss_true = self._loss(logits_real, Variable(self._labels))
        d_loss_true.backward()

        # Apply gradients for fake data
        d_loss_fake = self._loss(logits_fake,
                                 Variable(self._labels.fill_(0)))
        d_loss_fake.backward()

        return d_loss_true + d_loss_fake
