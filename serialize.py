import os
import torch

from torchgan.utils import get_files


DISCRIMINATOR_ID = 'netD'
GENERATOR_ID = 'netG'
DELIMITER = '_'
CHECKPOINT_FORMAT = '{id}' + DELIMITER + 'epoch' + DELIMITER + '{epoch}.pth'


def load_context(folder, epoch_num=None):
    """ Returns the loaded network using the checkpoints in the given folder.
    Set epoch number to None to load latest checkpoint. If no checkpoint found,
    returns None """
    checkpoints = _get_network_checkpoints(folder)

    if len(checkpoints) == 0:
        return None

    if epoch_num is None:
        loading_epoch = max(checkpoints, key=int)
    else:
        loading_epoch = epoch_num

    if loading_epoch not in checkpoints:
        raise ValueError('Cannot load epoch %d. Not found' % loading_epoch)

    disc_obj = torch.load(checkpoints[loading_epoch][DISCRIMINATOR_ID])
    gen_obj = torch.load(checkpoints[loading_epoch][GENERATOR_ID])

    return int(loading_epoch), gen_obj, disc_obj


def _split_checkpoint_path(f):
    file_name = os.path.basename(f)
    net_id, _, epoch = os.path.splitext(file_name)[0].split(DELIMITER)
    return net_id, epoch


def _get_network_checkpoints(folder):
    files = get_files(folder, extensions=['.pth'])
    checkpoints = {}

    for f in files:
        net_id, epoch = _split_checkpoint_path(f)

        if epoch not in checkpoints:
            checkpoints[epoch] = {}

        checkpoints[epoch][net_id] = f

    # Sanity check
    for epoch, paths in checkpoints.items():
        if len(paths.keys()) != 2 or set(paths.keys()) != \
                set([DISCRIMINATOR_ID, GENERATOR_ID]):
            raise IOError(
                'Generator and Discriminator endpoint not found for epoch '
            )

    return checkpoints
