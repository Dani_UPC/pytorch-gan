import os


HOME_FOLDER = os.path.expanduser("~")
DATA_ROOT = os.path.join(HOME_FOLDER, '.adversarial_data')
TMP_ROOT = os.path.join(HOME_FOLDER, '.adversarial_tmp')


class DataMode(object):

    TRAINING = 'training'
    VALIDATION = 'validation'
    TESTING = 'testing'


def get_output_folder():
    return '/media/walle/815d08cd-6bee-4a13-b6fd-87ebc1de2bb0/walle/training'


def get_data_location(dataset):
    return os.path.join(DATA_ROOT, dataset)


def get_subfolders(folder):
    """ Returns the folders contained in the input path """
    return [os.path.join(folder, name) for name in os.listdir(folder)
            if os.path.isdir(os.path.join(folder, name))]


def get_files(folder, extensions=None):
    return [
        os.path.join(folder, f) for f in os.listdir(folder)
        if os.path.isfile(os.path.join(folder, f)) and
        ((extensions is None) or (get_extension(f) in extensions))
    ]


def get_extension(f):
    return os.path.splitext(f)[1]


def create_dir(path):
    """ Creates directory if it does not exist """
    if not os.path.exists(path):
        os.makedirs(path)


def read_tab_map(path):
    """ Reads a file containing lines with two elements separated by a
    tab space: an id and its corresponding tag """
    with open(path) as f:
        content = f.readlines()

    mapped_ids = {}
    for line in content:
        num_id, name = line.strip().split('\t')
        mapped_ids[name] = int(num_id)
    return mapped_ids
